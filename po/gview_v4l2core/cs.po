# Czech translation for guvcview
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the guvcview package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: guvcview\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-06-01 23:47+0100\n"
"PO-Revision-Date: 2013-02-25 05:36+0000\n"
"Last-Translator: Michal Sittek <exxxo45@gmail.com>\n"
"Language-Team: Czech <cs@li.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-12-08 10:36+0000\n"
"X-Generator: Launchpad (build 16869)\n"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:73
msgid "Aperture Priority Mode"
msgstr "Režim priority clony"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:67
msgid "Auto"
msgstr "Automaticky"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:92
msgid "Auto Exposure"
msgstr "Automatická korekce závěrky"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:71
msgid "Auto Mode"
msgstr "Auto. mód"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:66
msgid "Backlight Compensation"
msgstr "Komponzace podsvětlení"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:74
msgid "Black Level"
msgstr "Úroveň černé"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:66
msgid "Blinking"
msgstr "Blikání"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:78
msgid "Blue Balance"
msgstr "Vyvážení modré"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:60
msgid "Brightness"
msgstr "Jas"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:91
msgid "Camera Controls"
msgstr "Ovládání kamery"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:86
msgid "Chroma AGC"
msgstr "AKP Sytosti"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:88
msgid "Color Effects"
msgstr "Barevné efekty"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:87
msgid "Color Killer"
msgstr "Odstranění barvy"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:61
msgid "Contrast"
msgstr "Kontrast"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:165
msgid "Disable video processing"
msgstr "Zakázat zpracování videa"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:76
msgid "Do White Balance"
msgstr "Vyvážit bílou"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:79
msgid "Exposure"
msgstr "Expozice"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:112
msgid "Exposure (Absolute)"
msgstr "Expozice (Absolutní)"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:93
msgid "Exposure Time, Absolute"
msgstr "Expoziční Doba, Absolutní"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:110
msgid "Exposure, Auto"
msgstr "Expozive, Auto"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:111
msgid "Exposure, Auto Priority"
msgstr "Expozice Auto. prorita"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:94
msgid "Exposure, Dynamic Framerate"
msgstr "Expozie, Dynamická rychlost snímků"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:120
#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:126
msgid "Focus"
msgstr "Zaostření"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:121
msgid "Focus (Absolute)"
msgstr "Ohnisko (Absolutní)"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:101
msgid "Focus, Absolute"
msgstr "Ohnisko, Absolutní"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:69
msgid "Focus, Auto"
msgstr "Ostření, Auto"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:103
msgid "Focus, Automatic"
msgstr "Zaostření, Automaticky"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:102
msgid "Focus, Relative"
msgstr "Ohnisko, Relativní"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:81
msgid "Gain"
msgstr "Zisk"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:80
msgid "Gain, Automatic"
msgstr "Přírůstek, Automatický"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:65
msgid "Gamma"
msgstr "Gamma"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:84
msgid "Horizontal Center"
msgstr "Horizontální střed"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:82
msgid "Horizontal Flip"
msgstr "Horizontální překlopení"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:62
msgid "Hue"
msgstr "Odstín"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:68
msgid "Hue, Automatic"
msgstr "Odstín, Automatický"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:152
msgid "LED1 Frequency"
msgstr "Frekvence LED1"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:139
msgid "LED1 Mode"
msgstr "Mód LED1"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:70
msgid "Manual Mode"
msgstr "Ruční mód"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:64
msgid "Off"
msgstr "Vypnuto"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:65
msgid "On"
msgstr "Zapnuto"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:74
msgid "Pan (relative)"
msgstr "Záběr (relativní)"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:100
msgid "Pan Reset"
msgstr "Reset záběru"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:99
msgid "Pan, Absolute"
msgstr "Sledování objektu, Absolutní"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:95
msgid "Pan, Relative"
msgstr "Sledování objektu, Relativní"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:97
msgid "Pan, Reset"
msgstr "Sledování objektu, Reset"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:67
msgid "Power Line Frequency"
msgstr "Kmitočet sítě"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:107
msgid "Privacy"
msgstr "Ochrana osobních údajů"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:178
msgid "Raw bits per pixel"
msgstr "Raw bity / pxel"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:77
msgid "Red Balance"
msgstr "Vyvážení červené"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:63
msgid "Saturation"
msgstr "Sytost"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:64
msgid "Sharpness"
msgstr "Ostrost"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:72
msgid "Shutter Priority Mode"
msgstr "Mód priority clony"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:87
msgid "Tilt (relative)"
msgstr "Záběr (relativní)"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_xu_ctrls.c:113
msgid "Tilt Reset"
msgstr "Obnovit"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:96
msgid "Tilt, Relative"
msgstr "Náklon, Relativní"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:98
msgid "Tilt, Reset"
msgstr "Náklon, Reset"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:59
msgid "User Controls"
msgstr "Uživatelká nastavení"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:85
msgid "Vertical Center"
msgstr "Vertikální střed"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:83
msgid "Vertical Flip"
msgstr "Vertikální překlopení"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:116
msgid "White Balance Blue Component"
msgstr "Modrá složka vyvážení bílé"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:115
msgid "White Balance Component, Auto"
msgstr "Komponenta vyvážení bílé, Auto"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:117
msgid "White Balance Red Component"
msgstr "Červená složka vyvážení bílé"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:114
msgid "White Balance Temperature"
msgstr "Vyvážení bílé"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:113
msgid "White Balance Temperature, Auto"
msgstr "Vyvážení bílé, Auto"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:75
msgid "White Balance, Automatic"
msgstr "Vyvážení bílé, automatické"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:104
msgid "Zoom, Absolute"
msgstr "Přiblížení, Absolutní"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:106
msgid "Zoom, Continuous"
msgstr "Přiblížení, Nepřetržité"

#: /home/paulo/Documentos/Devel/guvcview-git-master/gview_v4l2core/v4l2_controls.c:105
msgid "Zoom, Relative"
msgstr "Přiblížení, Relativní"

#~ msgid ""
#~ "\n"
#~ "You have more than one video device installed.\n"
#~ "Do you want to try another one ?\n"
#~ msgstr ""
#~ "\n"
#~ "Máte nainstalováno více než jedno zařízení pro video.\n"
#~ "Chcete zkusit další ?\n"

#~ msgid ""
#~ "                              encoder fps:   \n"
#~ " (0 - use fps combobox value)"
#~ msgstr ""
#~ "                              fps enkodéru:   \n"
#~ " (0 - použít hodnotu fps z pole)"

#~ msgid " Ducky"
#~ msgstr " Ducky"

#~ msgid " Echo"
#~ msgstr " Zobrazit"

#~ msgid " Fuzz"
#~ msgstr " Fuzz"

#~ msgid " Invert"
#~ msgstr " Invertovat"

#~ msgid " Mirror"
#~ msgstr " Zrcadlo"

#~ msgid " Mono"
#~ msgstr " Mono"

#~ msgid " Negative"
#~ msgstr " Negativ"

#~ msgid " Particles"
#~ msgstr " Částí"

#~ msgid " Pieces"
#~ msgstr " Kousků"

#~ msgid " Reverb"
#~ msgstr " Ozvěna"

#~ msgid " Show"
#~ msgstr " Zobrazit"

#~ msgid " Show VU meter"
#~ msgstr " Zobrazit metr VU"

#~ msgid " Sound"
#~ msgstr " Zvuk"

#~ msgid " WahWah"
#~ msgstr " WahWah"

#~ msgid " monotonic pts"
#~ msgstr " monotóních bodů"

#~ msgid "- local options"
#~ msgstr "- místní nastavení"

#~ msgid "---- Audio Effects ----"
#~ msgstr "---- Zvukové efekty ----"

#, fuzzy
#~ msgid "---- Audio Filters ----"
#~ msgstr "---- Filtry videa ----"

#~ msgid "---- Video Filters ----"
#~ msgstr "---- Filtry videa ----"

#~ msgid "1 - mono"
#~ msgstr "1 - mono"

#~ msgid "12 bit"
#~ msgstr "12 bitů"

#~ msgid "2 - stereo"
#~ msgstr "2 - stereo"

#~ msgid "8 bit"
#~ msgstr "8 bitů"

#~ msgid "A video viewer and capturer for the linux uvc driver"
#~ msgstr "Prohlížení a zachytávání videa z linuxových uvc ovladačů"

#~ msgid "ACC Low - (faac)"
#~ msgstr "Nizké ACC - (faac)"

#~ msgid "AVI - avi format"
#~ msgstr "AVI - formát avi"

#~ msgid ""
#~ "An error occurred while adding extension\n"
#~ "controls to the UVC driver\n"
#~ "Make sure you run guvcview as root (or sudo)."
#~ msgstr ""
#~ "Nastala chyba při přidání rozšířeného\n"
#~ "ovládání do ovladače UVC\n"
#~ "Ujistěte se, že spouštíte guvcview jako root (či sudo)."

#~ msgid "Audio"
#~ msgstr "Zvuk"

#~ msgid "Audio API:"
#~ msgstr "API audia:"

#, fuzzy
#~ msgid "Audio Controls"
#~ msgstr "Uživatelká nastavení"

#~ msgid "Auto Focus (continuous)"
#~ msgstr "Automatické ostření (pozvolné)"

#~ msgid "Camera Output:"
#~ msgstr "Výstup kamery:"

#~ msgid "Cap. Image"
#~ msgstr "Zazn. Obraz"

#~ msgid "Cap. Video"
#~ msgstr "Zazn. Video"

#~ msgid "Channels:"
#~ msgstr "Kanály:"

#~ msgid "Configuration file"
#~ msgstr "Konfigurační soubor"

#~ msgid "Dev. Default"
#~ msgstr "Výchozí zařízení"

#~ msgid "Device:"
#~ msgstr "Zařízení:"

#~ msgid "Displays debug information"
#~ msgstr "Zobrazí ladící informace"

#~ msgid "Dolby AC3 - (lavc)"
#~ msgstr "Dolby AC3 - (lavc)"

#~ msgid "Don't display a GUI"
#~ msgstr "Nezobrazovat rozhraní"

#~ msgid "Don't stream video (image controls only)"
#~ msgstr "Neposílat video (pouze kontroly obrázku)"

#~ msgid "Down"
#~ msgstr "Dolů"

#~ msgid "Error"
#~ msgstr "Chyba"

#~ msgid "Exit after adding UVC extension controls (needs root/sudo)"
#~ msgstr "Ukončit po přidání kontroly rozšíření UVC (vyžaduje root/sudo)"

#~ msgid "Exits guvcview after closing video"
#~ msgstr "Po skončení videa ukončit guvcview"

#~ msgid "Extension controls were added to the UVC driver"
#~ msgstr "Rozšířené ovládání bylo přidáno do UVC ovladače"

#~ msgid "FLV1 - flash video 1"
#~ msgstr "FLV1 - flash video 1"

#~ msgid "Focus (absolute)"
#~ msgstr "Ostření (absolutní)"

#~ msgid "Frame Rate:"
#~ msgstr "Snímkový kmitočet"

#~ msgid "Frame size, default: 640x480"
#~ msgstr "Velikost rámce, výchozí: 640x480"

#~ msgid "GTK UVC video viewer"
#~ msgstr "Prohlížeč videa GTK UVC"

#~ msgid "GUVCViewer Controls"
#~ msgstr "Ovládání GUVCViewer"

#, fuzzy
#~ msgid "Guvcview"
#~ msgstr "guvcview"

#~ msgid "Guvcview Video Capture"
#~ msgstr "Zachycení videa Gucview"

#~ msgid ""
#~ "Guvcview error:\n"
#~ "\n"
#~ "Can't set a valid video stream for guvcview"
#~ msgstr ""
#~ "Chyba Gucview:\n"
#~ "\n"
#~ "Nelze nastavit platný proud videa pro gucview"

#~ msgid ""
#~ "Guvcview error:\n"
#~ "\n"
#~ "Couldn't query device capabilities"
#~ msgstr ""
#~ "Chyba guvcview:\n"
#~ "\n"
#~ "Nelze zjistit funkce zařízení"

#~ msgid ""
#~ "Guvcview error:\n"
#~ "\n"
#~ "Read method error"
#~ msgstr ""
#~ "Chyba guvcview:\n"
#~ "\n"
#~ "Chyba metody čtení"

#~ msgid ""
#~ "Guvcview error:\n"
#~ "\n"
#~ "UVC Extension controls"
#~ msgstr ""
#~ "Chyba Guvcview\n"
#~ "\n"
#~ "Rozšířené ovládání UCV"

#~ msgid ""
#~ "Guvcview error:\n"
#~ "\n"
#~ "Unable to allocate Buffers"
#~ msgstr ""
#~ "Chyba Guvcview:\n"
#~ "\n"
#~ "Nelze alokovat Buffery"

#~ msgid ""
#~ "Guvcview error:\n"
#~ "\n"
#~ "Unable to create Video Thread"
#~ msgstr ""
#~ "Chyba Guvcview:\n"
#~ "\n"
#~ "Nelze založit vlákno videa"

#~ msgid ""
#~ "Guvcview error:\n"
#~ "\n"
#~ "Unable to open device"
#~ msgstr ""
#~ "ChybaGuvcview:\n"
#~ "\n"
#~ "Nelze otevřít zařízení"

#~ msgid ""
#~ "Guvcview error:\n"
#~ "\n"
#~ "Unable to start with minimum setup"
#~ msgstr ""
#~ "Chyba Guvcview:\n"
#~ "\n"
#~ "Nelze spustit minimální konfguraci"

#, fuzzy
#~ msgid ""
#~ "Guvcview version %s\n"
#~ "\n"
#~ msgstr "Varování Guvcview:"

#~ msgid ""
#~ "Guvcview:\n"
#~ "\n"
#~ "UVC Extension controls"
#~ msgstr ""
#~ "Guvcview\n"
#~ "\n"
#~ "Rozšířené ovládání UVC"

#~ msgid "Hardware accelaration (enable(1) | disable(0))"
#~ msgstr "Hardwarová akcelerace (povolit(1) | zakázat(0))"

#~ msgid "Image Controls"
#~ msgstr "Ovládání obrazů"

#~ msgid "Image capture interval in seconds"
#~ msgstr "Interval snímání v sekundách"

#~ msgid "Input Device:"
#~ msgstr "Vstupní zařízení:"

#~ msgid "Left"
#~ msgstr "Vlevo"

#~ msgid "Load Profile at start"
#~ msgstr "Načíst profil při spuštění"

#~ msgid "MJPG - compressed"
#~ msgstr "MJPG - komprimovaný"

#~ msgid "MKV - Matroska format"
#~ msgstr "MKV - formát Matroska"

#~ msgid "MP3 - (lavc)"
#~ msgstr "MP3 - (lavc)"

#~ msgid "MPEG video 1"
#~ msgstr "MPEG video 1"

#~ msgid "MPEG2 - (lavc)"
#~ msgstr "MPEG2 - (lavc)"

#~ msgid "MPEG4-ASP"
#~ msgstr "MPEG4-ASP"

#~ msgid "MPEG4-AVC (H264)"
#~ msgstr "MPEG4-AVC (H264)"

#~ msgid "MPG2 - MPG2 format"
#~ msgstr "MPG2 - formát MPG2"

#~ msgid "MS MP4 V3"
#~ msgstr "MS MP4 V3"

#~ msgid "Make sure the device driver supports v4l2."
#~ msgstr "Ujistěte se, že ovladač zařízení podporuje v4l2."

#~ msgid ""
#~ "Make sure your device driver is v4l2 compliant\n"
#~ "and that it is properly installed."
#~ msgstr ""
#~ "Ujistěte se, že ovladač vašeho přístroje je kompatibilní s 4l2\n"
#~ "a že je správně nainstalován."

#~ msgid "Not enough free space left on disk"
#~ msgstr "Na disku nezbývá dost volného místa"

#~ msgid "Number of Pictures to capture"
#~ msgstr "Počet obrázků k zaznamenání"

#~ msgid "Number of initial frames to skip"
#~ msgstr "Počet prvních vynechaných snímků"

#, fuzzy
#~ msgid "PCM - uncompressed (float 32 bit)"
#~ msgstr "PCM - bez komprese (16 bitů)"

#~ msgid "PORTAUDIO"
#~ msgstr "PORTAUDIO"

#~ msgid "PULSEAUDIO"
#~ msgstr "PULSEAUDIO"

#, fuzzy
#~ msgid "Photo file name"
#~ msgstr "Název souboru orazu"

#~ msgid ""
#~ "Pixel format(mjpg|jpeg|yuyv|yvyu|uyvy|yyuv|yu12|yv12|nv12|nv21|nv16|nv61|"
#~ "y41p|grey|y10b|y16 |s501|s505|s508|gbrg|grbg|ba81|rggb|bgr3|rgb3)"
#~ msgstr ""
#~ "Formát pixelu(mjpg|jpeg|yuyv|yvyu|uyvy|yyuv|yu12|yv12|nv12|nv21|nv16|nv61|"
#~ "y41p|grey|y10b|y16 |s501|s505|s508|gbrg|grbg|ba81|rggb|bgr3|rgb3)"

#~ msgid ""
#~ "Please make sure the camera is connected\n"
#~ "and that the correct driver is installed."
#~ msgstr ""
#~ "Ujistěte se prosím, že vaše kamera je připojena,\n"
#~ "a že máte nainstalován správný ovladač."

#~ msgid "Please reconnect your camera."
#~ msgstr "Prosím připojte znovu kameru."

#~ msgid "Please report it to http://developer.berlios.de/bugs/?group_id=8179"
#~ msgstr ""
#~ "Prosím nahlašte to na http://developer.berlios.de/bugs/?group_id=8179"

#~ msgid "Please try mmap instead (--capture_method=1)."
#~ msgstr "Místo (--capture_method=1) zkuste mmap."

#~ msgid "Please try restarting your system."
#~ msgstr "Prosím zkuste restartovat váš systém."

#, fuzzy
#~ msgid "Print version"
#~ msgstr "Vypíše verzi"

#~ msgid "Quality:"
#~ msgstr "Kvalita:"

#~ msgid "RGB - uncomp BMP"
#~ msgstr "RGB - uncomp BMP"

#, fuzzy
#~ msgid "Raw camera input"
#~ msgstr "Výstup kamery:"

#~ msgid "Resolution:"
#~ msgstr "Rozlišení:"

#~ msgid "Right"
#~ msgstr "Vpravo"

#~ msgid "Sample Rate:"
#~ msgstr "Vzorkovací frekvence:"

#~ msgid "Save File"
#~ msgstr "Uložit soubor"

#, fuzzy
#~ msgid "Set capture method [read | mmap (def)]"
#~ msgstr "Metoda zachycení (1-mmap (výchozí) 2-read)"

#, fuzzy
#~ msgid "Set device name (def: /dev/video0)"
#~ msgstr "Video zařízení [výchozí /dev/video0]"

#~ msgid "Show FPS value (enable(1) | disable (0))"
#~ msgstr "Zobrazit hodnotu FPS (povolit(1) | zakázat(0))"

#~ msgid "Up"
#~ msgstr "Nahoru"

#, fuzzy
#~ msgid "Video Controls"
#~ msgstr "Uživatelká nastavení"

#~ msgid "Video File name (capture from start)"
#~ msgstr "Jméno Video souboru (záznam od začátku)"

#~ msgid "Video capture time (in seconds)"
#~ msgstr "Čas Video záznamu (v sekundách)"

#, fuzzy
#~ msgid "Video file name"
#~ msgstr "Název souboru orazu"

#~ msgid "WMV1 - win. med. video 7"
#~ msgstr "WM1 - windows media video 7"

#~ msgid "YUY2 - uncomp YUV"
#~ msgstr "YUY2 - uncomp YUV"

#, fuzzy
#~ msgid "_Apply"
#~ msgstr "Použít"

#, fuzzy
#~ msgid "_Cancel"
#~ msgstr "zrušit"

#~ msgid "audio codec values"
#~ msgstr "hodnoty audio kodeku"

#~ msgid "bit rate:   "
#~ msgstr "přenosová rychlost:   "

#~ msgid "cancel"
#~ msgstr "zrušit"

#~ msgid "cmp:   "
#~ msgstr "cmp:   "

#~ msgid "codec values"
#~ msgstr "hodnoty kodeku"

#~ msgid "dia size:   "
#~ msgstr "velikost dia:   "

#~ msgid "framerefs:   "
#~ msgstr "framerefs:   "

#~ msgid "gop size:   "
#~ msgstr "velikost gop:   "

#~ msgid "guvcview"
#~ msgstr "guvcview"

#~ msgid "last predictor count:   "
#~ msgstr "poslední počet prediktoru:   "

#~ msgid ""
#~ "launch new process or restart?.\n"
#~ "\n"
#~ msgstr ""
#~ "Spustit nový proces nebo restartovat?.\n"
#~ "\n"

#~ msgid "max B frames:   "
#~ msgstr "max B rámečků:   "

#~ msgid "max. qdiff:   "
#~ msgstr "max. qdiff:   "

#~ msgid "mb decision:   "
#~ msgstr "rozhodnutí mb:   "

#~ msgid "me method:   "
#~ msgstr "metoda me:   "

#~ msgid "new"
#~ msgstr "nový"

#~ msgid "num threads:   "
#~ msgstr "počet vláken:   "

#~ msgid "pre cmp:   "
#~ msgstr "pre cmp:   "

#~ msgid "pre dia size:   "
#~ msgstr "velikost pre dia:   "

#~ msgid "pre me:   "
#~ msgstr "pre me:   "

#~ msgid "qblur:   "
#~ msgstr "qrozmazání:   "

#~ msgid "qcompress:   "
#~ msgstr "qkomprese:   "

#~ msgid "qmax:   "
#~ msgstr "qmax:   "

#~ msgid "qmin:   "
#~ msgstr "qmin:   "

#~ msgid "restart"
#~ msgstr "restart"

#~ msgid "set Focus"
#~ msgstr "nastavit Ostření"

#~ msgid "start new"
#~ msgstr "spustit nový"

#~ msgid "sub cmp:   "
#~ msgstr "pod cmp:   "

#~ msgid "subq:   "
#~ msgstr "subq:   "

#, fuzzy
#~ msgid "video codec values"
#~ msgstr "hodnoty audio kodeku"
